import { createFeatureSelector, createSelector, select } from "@ngrx/store";
import { FormState, IUserData } from "./form.reducer";

export const formKey = "form" as const;

export const selectForm = createFeatureSelector<FormState>(formKey);

export const selectUserData = createSelector(
  selectForm,
  (state: FormState) => state.userData
);

export const selectUsername = createSelector(
  selectUserData,
  (state: IUserData) => state.username
);

export const selectPassword = createSelector(
  selectUserData,
  (state: IUserData) => state.password
);
