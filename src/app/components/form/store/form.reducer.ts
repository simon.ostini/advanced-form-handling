import { createReducer, on } from "@ngrx/store";
import { setUserData } from "./form.actions";

export interface IUserData {
  username: string;
  password: string;
}

export interface FormState {
  userData: IUserData;
}

export const initialState: FormState = {
  userData: <IUserData>{ username: "simon", password: "test" },
} as const;

export const formReducer = createReducer(
  initialState,
  on(setUserData, (state: FormState, action: IUserData) => {
    return {
      ...state,
      userData: action,
    };
  })
);
