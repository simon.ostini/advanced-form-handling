import { createAction, props } from "@ngrx/store";
import { IUserData } from "./form.reducer";

export const setUserData = createAction(
  "[User Data] set user data",
  props<IUserData>()
);
