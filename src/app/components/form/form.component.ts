import { AfterViewInit, Component, OnInit, ViewChild } from "@angular/core";
import { NgForm } from "@angular/forms";
import { Store } from "@ngrx/store";
import { Observable, of } from "rxjs";
import { setUserData } from "./store/form.actions";
import { IUserData } from "./store/form.reducer";
import { selectUserData } from "./store/form.selectors";

@Component({
  selector: "app-form",
  templateUrl: "./form.component.html",
  styleUrls: ["./form.component.scss"],
})
export class FormComponent implements OnInit, AfterViewInit {
  public userData$!: Observable<IUserData>;
  @ViewChild("form") public form!: NgForm;

  constructor(private _formStore: Store<any>) {
    this.userData$ = this._formStore.select((state) => selectUserData(state));

    this.userData$.subscribe((userData: IUserData) => console.log(userData));
  }

  ngAfterViewInit(): void {
    console.log(this.form.form);
  }

  public onSubmit(): void {
    this._formStore.dispatch(
      setUserData({
        username: this.form.form.controls.username.value,
        password: this.form.form.controls.password.value,
      })
    );
  }

  ngOnInit(): void {}
}
